// Created with Motiva Layama v1.18b https://www.motivacg.com

function getLayamaCameras()
{
   var layamaCameras = new BABYLON.SmartArray(0);
   layamaCameras.push({n: "Track0011", p: new BABYLON.Vector3(115153, 109861, 111473), l: new BABYLON.Vector3(115151, 109861, 111473)});
   layamaCameras.push({n: "Track0000", p: new BABYLON.Vector3(111356, 109861, 114427), l: new BABYLON.Vector3(111356, 109861, 114428)});
   layamaCameras.push({n: "Track0001", p: new BABYLON.Vector3(111485, 109861, 114115), l: new BABYLON.Vector3(111485, 109861, 114116)});
   layamaCameras.push({n: "Track0002", p: new BABYLON.Vector3(111682, 109861, 113742), l: new BABYLON.Vector3(111681, 109861, 113743)});
   layamaCameras.push({n: "Track0003", p: new BABYLON.Vector3(111929, 109861, 113332), l: new BABYLON.Vector3(111928, 109861, 113333)});
   layamaCameras.push({n: "Track0004", p: new BABYLON.Vector3(112210, 109861, 112965), l: new BABYLON.Vector3(112209, 109861, 112966)});
   layamaCameras.push({n: "Track0005", p: new BABYLON.Vector3(112556, 109861, 112608), l: new BABYLON.Vector3(112555, 109861, 112609)});
   layamaCameras.push({n: "Track0006", p: new BABYLON.Vector3(112925, 109861, 112313), l: new BABYLON.Vector3(112925, 109861, 112314)});
   layamaCameras.push({n: "Track0007", p: new BABYLON.Vector3(113331, 109861, 112047), l: new BABYLON.Vector3(113330, 109861, 112048)});
   layamaCameras.push({n: "Track0008", p: new BABYLON.Vector3(113760, 109861, 111829), l: new BABYLON.Vector3(113759, 109861, 111830)});
   layamaCameras.push({n: "Track0009", p: new BABYLON.Vector3(114219, 109861, 111662), l: new BABYLON.Vector3(114218, 109861, 111662)});
   layamaCameras.push({n: "Track0010", p: new BABYLON.Vector3(114699, 109861, 111542), l: new BABYLON.Vector3(114698, 109861, 111542)});
   return layamaCameras;
}

function getLayamaResolutions()
{
   var layamaResolutions = new BABYLON.SmartArray(0);
   layamaResolutions.push("2048");
   layamaResolutions.push("1024");
   return layamaResolutions;
}

function getOnScreenLogoUsage()
{
   return 6;
}

function getLayamaControls()
{
   return {defMove: true, defRot: 0, altMove: true, altRot: 1};
}

